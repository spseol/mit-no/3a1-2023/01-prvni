#include "stm8s.h"
#include "main.h"
#include "milis.h"
#include "daughterboard.h"

//#include "delay.h"
//#include <stdio.h>
//#include "uart1.h"


void setup(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);      // taktovani MCU na 16MHz

    GPIO_Init(LED6_PORT, LED6_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(LED7_PORT, LED7_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(LED8_PORT, LED8_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(PWMR_PORT,PWMR_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(PWMG_PORT,PWMG_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(PWMB_PORT,PWMB_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(S1_PORT, S1_PIN, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(S2_PORT, S2_PIN, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(S3_PORT, S3_PIN, GPIO_MODE_IN_PU_NO_IT);
    
    init_milis();
    //init_uart1();
}


int main(void)
{
    
    uint32_t time6 = 0;
    uint32_t time7 = 0;
    uint32_t time8 = 0;
    uint32_t time_btn = 0;
    uint32_t time_led = 0;
    uint32_t counter = 0;

    
    uint8_t btn_down = 0;   // detekuji, že tlačítko je stisknuto

    setup();

    while (1) {

        /*
        if (GPIO_ReadInputPin(S3_PORT, S3_PIN) == 0) {
            HIGH(PWMR);
            HIGH(PWMG);
            HIGH(PWMB);
        } else {
            LOW(PWMR);
            LOW(PWMG);
            LOW(PWMB);
        }
        */
        
        
        if (milis() - time_btn > 20 ) {
            time_btn = milis();
            if ( PUSH(S1)) {
                btn_down = 1;
                counter++;
            } else {
                if (btn_down == 1) { // ted sem pustil tlacitko
                    HIGH(PWMR);
                    time_led = milis();
                }
                btn_down = 0;
            }
        }
        
        if (milis() - time_led > 100) {
            if (READ(PWMR)) {
                if (counter > 0) {
                    counter--;
                } else {
                    LOW(PWMR);
                }

            }
        }

        if (milis() - time6 > 999 ) {
            REVERSE(LED6);
            time6 = milis();
        }
        if (milis() - time7 > 777 ) {
            REVERSE(LED7);
            time7 = milis();
        }
        if (milis() - time8 > 888 ) {
            REVERSE(LED8);
            time8 = milis();
        }

    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
